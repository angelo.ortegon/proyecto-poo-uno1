@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
                <div class="card-header">LISTA DE PRODUCTOS</div>

                <div class="col text-right">
                <a class="btn btn-primary" href="{{ route ('crear.producto')}}" role="button">Ingresar Producto</a>

                    
            </div>

                <div class="card-body">

                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Tipo </th>
                        <th scope="col">Estado</th>
                        <th scope="col">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($producto as $item)
                    
                       <tr>
                        <th scope="row">{{$item->id}}</th>
                        <td>{{$item->Nombre}}</td>
                        <td>{{$item->Tipo}}</td>
                        <td>{{$item->Estado}}</td>
                        <td>{{$item->Precio}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                    </table>
                                        
                                            </div>
                                        
                                            <div class="card-body">
                </div>
                <div class="col text-right">
                         <a class="btn btn-primary" href="{{ url('/home') }}">Cancelar</a>
                </div>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection
