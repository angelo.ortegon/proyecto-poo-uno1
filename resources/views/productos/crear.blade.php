@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
                <div class="card-header">CREAR PRODUCTO</div>

                <div class="col text-right">
                    <a class="btn btn-primary" href="{{ route ('lista.producto')}}" role="button">Cancelar</a>
                </div>
                            
                <div class="card-body">

                 <form role="form" method="post" action="{{ route('guardar.productos')}}">
                    {{ csrf_field () }}
                    {{ method_field ('post') }}

                 <div class="row">
                    <div class="col-lg-4">
                        <label class="from-control-label" for="nombre">Nombre del producto</label>
                        <input type="text" class="from-control" name="nombre">
                        </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="tipo">Tipo del producto</label>
                        <input type="text" class="from-control" name="tipo">
                        </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="estado">Estado del producto</label>
                        <input type="number" class="from-control" name="estado">
                        </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="precio">Precio del producto</label>
                        <input type="number" class="from-control" name="precio">
                        </div>

                        
                        </div>
                        <div class="card-body">
                        
                    <button type="submit" class="btn btn-success pull-right">Guardar</button>
                </form>
                                            
                      

    



                </div>

                
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection