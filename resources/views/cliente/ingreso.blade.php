@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
                <div class="card-header">NUEVO CLIENTE</div>

                <div class="col text-right">
                    <a class="btn btn-primary" href="{{ route ('ingreso.cliente')}}" role="button">Nuevo cliente</a>
                </div>
                     
                <div class="card-body">
                </div>
                <div class="col text-right">
                    <a class="btn btn-primary" href="{{ route ('lista.cliente')}}" role="button">Cancelar</a>
                </div>   
                
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection