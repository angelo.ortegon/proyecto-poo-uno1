<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home',['as'=>'.home', 'uses'=>'HomeController@index'])->name('home');

Route::group(['middleware' => ['auth']], function() {

    Route::post('/guardar/productos', ['as'=>'guardar.productos', 'uses'=>'ProductoController@GuardarProducto']);
    Route::get('/lista/productos', ['as'=>'lista.producto', 'uses'=>'ProductoController@InicioProducto']);
    Route::get('/crear/productos', ['as'=>'crear.producto', 'uses'=>'ProductoController@CrearProducto']);
    Route::get('/lista/cliente', ['as'=>'lista.cliente', 'uses'=>'ClienteController@ListaCliente']);
    Route::get('/ingreso/cliente', ['as'=>'ingreso.cliente', 'uses'=>'ClienteController@IngresoCliente']);

});



